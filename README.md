# WHAT
Chat app based in React with typescript.
Based on Redux to manage the state and React-router for routing.
Worked with Yarn.
Eslint, prettier for readibility of the code.

# API REST
Must have npm installed:
Linux: sudo apt install npm
Install json-server:
npm install -g json-server
And run:
json-server --watch db.json --port 3004

Json with the data is in db.json based in the src folder.


## RUN
Running with yarn start.
After started, the app will show the home page and you can acces the chat review screen by the different links at the page.

## NOT DONE
- Search doesn't work. Is there for the design (required).

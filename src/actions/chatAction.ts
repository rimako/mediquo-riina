import * as types from '../constants/ActionTypes';
import { ConservationType } from '../State/ConversationType';
import { Message } from '../State/Message';

export const fetchChats = () => {
    return (
        dispatch: (arg0: { type: string; result: ConservationType[] }) => void,
    ) => {
        fetch('http://localhost:3004/chat')
            .then((res) => res.json())
            .then((result) => {
                let convList: ConservationType[] = result;
                for (const conv of convList) {
                    if (conv.messages !== null) {
                        conv.messages.sort(function (a: Message, b: Message) {
                            const keyA = new Date(a.sent_at),
                                keyB = new Date(b.sent_at);
                            if (keyA < keyB) return -1;
                            if (keyA > keyB) return 1;
                            return 0;
                        });
                    }
                }
                convList.sort(function (a: ConservationType, b: ConservationType) {
                    if (!a.messages) {
                        return 0;
                    }
                    if (!b.messages) {
                        return 0;
                    }
                    const keyA = new Date(a.messages[a.messages.length - 1].sent_at),
                        keyB = new Date(b.messages[b.messages.length - 1].sent_at);
                    if (keyA < keyB) return 1;
                    if (keyA > keyB) return -1;
                    return 0;
                });
                convList = convList.map((conversation) => {
                    return {
                        ...conversation,
                        lastAt: conversation.messages && conversation.messages[0].sent_at,
                    };
                });
                dispatch({
                    type: types.FETCH_DATA,
                    result: convList,
                });
            });
    };
};

export const selectChat = (chat: ConservationType) => ({
    type: types.SELECT_CHAT,
    chat,
});
export const changeStatus = (status: string) => ({
    type: types.CHANGE_STATUS,
    status,
});
import React from 'react';

import './ConversationListItem.css';
import { useDispatch, useSelector } from 'react-redux';
import { selectChat } from '../../actions/chatAction';
import { ConservationType } from '../../State/ConversationType';

export default function ConversationListItem(props: {
	conversation: ConservationType;
}) {
	const selected = useSelector((state: any) => state.chat.selected);
	const { messages, user, lastAt, room_id } = props.conversation;
	const dispatch = useDispatch();
	return (
		<div
			onClick={() => dispatch(selectChat(props.conversation))}
			className={
				(room_id === selected ? 'selected ' : '') + 'conversation-list-item'
			}
		>
			<img
				className="conversation-photo"
				src={user.avatar}
				alt="conversation"
			/>
			<div className="conversation-info">
				<h1 className="conversation-title">
					{user.name}
					<span className="conversation-hour">
						{lastAt &&
							lastAt
								.toString()
								.slice(lastAt.toString().length - 8, lastAt.toString().length)}
					</span>
				</h1>
				<p className="conversation-snippet">
					{messages && messages[messages.length - 1].message}
				</p>
			</div>
		</div>
	);
}

import React from 'react';
import './ConversationSearch.css';
import SearchIcon from '@material-ui/icons/Search';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';

export default function ConversationSearch() {
	return (
		<div className="conversation-search">
			<Input
				startAdornment={
					<InputAdornment position="start">
						<SearchIcon />
					</InputAdornment>
				}
				type="search"
				placeholder="Search"
				className="conversation-search-input"
			/>
		</div>
	);
}

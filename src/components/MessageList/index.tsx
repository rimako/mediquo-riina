import React from 'react';
import ShowMessage from '../Message';
import moment from 'moment';
import { useSelector } from 'react-redux';

import './MessageList.css';
import { ConservationType } from '../../State/ConversationType';
import MessageHeader from './MessageHeader';

export default function MessageList() {
    const selectedConversation: ConservationType = useSelector(
        (state: any) => state.chat.selectedConversation,
    );

    const renderMessages = () => {
        if (!selectedConversation) {
            return null;
        }
        const messages = selectedConversation.messages || [];
        let i = 0;
        const tempMessages = [];

        while (i < messages.length) {
            const previous = messages[i - 1];
            const current = messages[i];
            const next = messages[i + 1];
            const isProfessional = current.sender_id === current.professional.user_id;
            const currentMoment = moment(current.sent_at);
            let prevBySameAuthor = false;
            let nextBySameAuthor = false;
            let startsSequence = true;
            let endsSequence = true;
            let showTimestamp = true;

            if (previous) {
                const previousMoment = moment(previous.sent_at);
                const previousDuration = moment.duration(
                    currentMoment.diff(previousMoment),
                );
                prevBySameAuthor = previous.sender_id === current.sender_id;

                if (prevBySameAuthor && previousDuration.as('hours') < 1) {
                    startsSequence = false;
                }

                if (previousDuration.as('hours') < 1) {
                    showTimestamp = false;
                }
            }

            if (next) {
                const nextMoment = moment(next.sent_at);
                const nextDuration = moment.duration(nextMoment.diff(currentMoment));
                nextBySameAuthor = next.sender_id === current.sender_id;

                if (nextBySameAuthor && nextDuration.as('hours') < 1) {
                    endsSequence = false;
                }
            }

            tempMessages.push(
                <ShowMessage
                    key={i}
                    isProfessional={isProfessional}
                    startsSequence={startsSequence}
                    endsSequence={endsSequence}
                    showTimestamp={showTimestamp}
                    data={current}
                />,
            );
            i += 1;
        }

        return tempMessages;
    };

    return (
        <div className="message-list">
            {selectedConversation && (
                <>
                    <MessageHeader
                        userName={selectedConversation.user.name}
                        status={selectedConversation.reviewStatus}
                    />
                    <div className="message-list-container">{renderMessages()}</div>
                </>
            )}
        </div>
    );
}

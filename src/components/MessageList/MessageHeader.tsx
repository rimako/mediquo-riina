import React from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {
    createStyles,
    Theme,
    makeStyles,
} from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';
import { changeStatus } from '../../actions/chatAction';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        title: {
            flexGrow: 1,
            display: 'none',
            [theme.breakpoints.up('sm')]: {
                display: 'block',
            },
        },
    }),
);

export default function MessageHeader(props: {
    userName: string;
    status?: string;
}) {
    const name = props.userName;
    const status = props.status;
    const classes = useStyles();
    const dispatch = useDispatch();


    return (
        <div className={classes.root}>
            <header>
                <Toolbar>
                    <Typography className={classes.title} variant="h6" noWrap>
                        {name}
                    </Typography>
                    <select
                        value={status || ''}
                        onChange={(ev) => dispatch(changeStatus(ev.target.value))}
                    >
                        <option value={''} disabled>
                            Not Reviewed
						</option>
                        <option value={'good'}>GOOD</option>
                        <option value={'bad'}>BAD</option>
                    </select>
                </Toolbar>
            </header>
        </div>
    );
}

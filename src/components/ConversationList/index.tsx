import React, { useEffect } from 'react';
import ConversationSearch from '../ConversationSearch';
import ConversationListItem from '../ConversationListItem';
import { useDispatch, useSelector } from 'react-redux';

import './ConversationList.css';
import { ConservationType } from '../../State/ConversationType';
import { fetchChats } from '../../actions/chatAction';

export default function ConversationList() {
	const conversationList = useSelector(
		(state: any) => state.chat.conversationList,
	);
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(fetchChats());

		// eslint-disable-next-line
	}, []);

	return (
		<div className="conversation-list">
			<ConversationSearch />
			{conversationList &&
				conversationList.map((conversation: ConservationType) => (
					<ConversationListItem
						key={conversation.room_id}
						conversation={conversation}
					/>
				))}
		</div>
	);
}

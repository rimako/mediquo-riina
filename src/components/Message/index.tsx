import React from 'react';
import moment from 'moment';
import './Message.css';
import { Message } from '../../State/Message';

export default function ShowMessage(props: {
	data: Message;
	isProfessional: boolean;
	startsSequence: boolean;
	endsSequence: boolean;
	showTimestamp: boolean;
}) {
	const {
		data,
		isProfessional,
		startsSequence,
		endsSequence,
		showTimestamp,
	} = props;

	const friendlyTimestamp = moment(data.sent_at).format('LLLL');
	return (
		<div
			className={[
				'message',
				`${isProfessional ? 'professional' : ''}`,
				`${startsSequence ? 'start' : ''}`,
				`${endsSequence ? 'end' : ''}`,
			].join(' ')}
		>
			{showTimestamp && <div className="timestamp">{friendlyTimestamp}</div>}

			<div className="bubble-container">
				<div className="bubble" title={friendlyTimestamp}>
					{data.message}
				</div>
			</div>
		</div>
	);
}

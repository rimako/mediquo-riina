import {
    CHANGE_STATUS,
    FETCH_DATA,
    SELECT_CHAT,
} from '../constants/ActionTypes';
import { ChatState } from '../State/State';
import { ConservationType } from '../State/ConversationType';

const initialState: ChatState = {
    conversationList: [],
    loading: false,
    selected: null,
    selectedConversation: null,
};

export default function chat(
    state = initialState,
    action: { type: any; result: ConservationType[]; chat: ConservationType; status: string },
) {
    switch (action.type) {
        case FETCH_DATA: {
            return {
                ...state,
                conversationList: action.result,
            };
        }
        case SELECT_CHAT:
            return {
                ...state,
                selected: action.chat.room_id,
                selectedConversation: action.chat,
            };

        case CHANGE_STATUS:
            return {
                ...state,
                selectedConversation: {
                    ...state.selectedConversation,
                    reviewStatus: action.status
                },
                conversationList: state.conversationList.map(
                    (conver) => {
                        if (conver.room_id === state.selectedConversation?.room_id) {
                            return { ...conver, reviewStatus: action.status }
                        } else {
                            return { ...conver };
                        }
                    }
                )
            };
        default:
            return state;
    }
}

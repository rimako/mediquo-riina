import { User } from './User';
import { Message } from './Message';

export class ConservationType {
    'room_id': string;
    'user': User;
    'messages': Message[];
    'lastAt'?: Date;
    'reviewStatus'?: 'good' | 'bad' | '';
}

import { Person } from './Person';

export class Message {
	'message_id': string;
	'sender_id': string;
	'message': string;
	'professional': Person;
	'patient': Person;
	'sent_at': Date;
}

import { ConservationType } from './ConversationType';

export interface ChatState {
	conversationList: ConservationType[];
	selected: string | null;
	loading: boolean;
	selectedConversation: ConservationType | null;
}
